package mx.tecnm.misantla.appitsav3.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import mx.tecnm.misantla.appitsav3.R
import mx.tecnm.misantla.appitsav3.databinding.ItemPokemonBinding
import mx.tecnm.misantla.appitsav3.model.Pokemon

class PokemonAdapter(var pokemons:MutableList<Pokemon>, val itemCallback: (item:Pokemon)->Unit):
    RecyclerView.Adapter<PokemonAdapter.PokemonAdapterViewHolder>() {

    class  PokemonAdapterViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val binding:ItemPokemonBinding = ItemPokemonBinding.bind(itemView)
        fun bind(pokemon: Pokemon){
            binding.tvNombrePokemon.text = pokemon.nombre
            Picasso.get().load(pokemon.urlImagen).error(R.drawable.pokemon).into(binding.imgPokemon)
        }
    }
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PokemonAdapter.PokemonAdapterViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_pokemon,parent,false)
        return PokemonAdapterViewHolder(view)
    }

    override fun onBindViewHolder(holder: PokemonAdapter.PokemonAdapterViewHolder, position: Int) {
        val pokemon = pokemons[position]
        holder.bind(pokemon)

        holder.itemView.setOnClickListener {
            itemCallback(pokemon)
        }

    }

    override fun getItemCount(): Int {
        return pokemons.size
    }

}