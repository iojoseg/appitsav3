package mx.tecnm.misantla.appitsav3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.squareup.picasso.Picasso
import mx.tecnm.misantla.appitsav3.databinding.ActivityDetallePokemonBinding
import mx.tecnm.misantla.appitsav3.databinding.ActivityMainBinding
import mx.tecnm.misantla.appitsav3.model.Pokemon

class DetallePokemonActivity : AppCompatActivity() {
 lateinit var binding: ActivityDetallePokemonBinding
        override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetallePokemonBinding.inflate(layoutInflater)
        setContentView(binding.root)

            val bundle:Bundle? = intent.extras
            bundle?.let {
                val pokemon = it.getSerializable("key_pokemon") as Pokemon
                binding.textView.text = pokemon.nombre
                Picasso.get().load(pokemon.urlImagen).into(binding.imageView)
            }

    }
}

