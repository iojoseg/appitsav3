package mx.tecnm.misantla.appitsav3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import mx.tecnm.misantla.appitsav3.adapter.PokemonAdapter
import mx.tecnm.misantla.appitsav3.databinding.ActivityMainBinding
import mx.tecnm.misantla.appitsav3.model.Pokemon


class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding
    val pokemos = mutableListOf<Pokemon>()
    lateinit var adaptador:PokemonAdapter

   override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

       cargaInformacion()
       configurarAdaptador()
    }

    private fun configurarAdaptador() {
        adaptador = PokemonAdapter(pokemos){
            Toast.makeText(this,it.nombre,Toast.LENGTH_SHORT).show()
            val bundle = Bundle().apply {
              //putString("key_nombre",it.nombre)
                //pasar todo el  objeto completo
                putSerializable("key_pokemon",it)
            }

            val intent = Intent(this,DetallePokemonActivity::class.java).apply {
               putExtras(bundle)
            }
            startActivity(intent)
        }
        binding.recyclerviewPokemon.adapter = adaptador
        binding.recyclerviewPokemon.layoutManager = LinearLayoutManager(this)

    }

    private fun cargaInformacion() {
        pokemos.add(Pokemon("Bulbasaur","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png"))
        pokemos.add(Pokemon("Ivysaur","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/2.png"))
        pokemos.add(Pokemon("Venusaur","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/3.png"))
        pokemos.add(Pokemon("Charmander","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/4.png"))
        pokemos.add(Pokemon("Charmeleon","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/5.png"))
        pokemos.add(Pokemon("Charizard","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/6.png"))
        pokemos.add(Pokemon("Squirtle","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/7.png"))
        pokemos.add(Pokemon("Wartortle","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/8.png"))
        pokemos.add(Pokemon("Blastoise","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/9.png"))
        pokemos.add(Pokemon("Caterpie","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/10.png"))
        pokemos.add(Pokemon("Metapod","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/11.png"))
        pokemos.add(Pokemon("Butterfree","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/12.png"))
        pokemos.add(Pokemon("Weedle","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/13.png"))
        pokemos.add(Pokemon("Kakuna","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/14.png"))
        pokemos.add(Pokemon("Beedrill","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/15.png"))
        pokemos.add(Pokemon("Pidgey","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/16.png"))
        pokemos.add(Pokemon("Pidgeotto","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/17.png"))
        pokemos.add(Pokemon("Pidgeot","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/18.png"))
        pokemos.add(Pokemon("Rattata","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/19.png"))
        pokemos.add(Pokemon("Raticate","https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/20.png"))
    }
}