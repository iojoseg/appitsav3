package mx.tecnm.misantla.appitsav3.model

import java.io.Serializable

data class Pokemon(
    val nombre:String,
    val urlImagen:String
    ) : Serializable {
}